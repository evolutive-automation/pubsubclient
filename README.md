# 啟動步驟
* 打包
```shell
mvn clean install
```
* 啟動
```shell
cd ./target
# 預設spring.profiles.active=dev，執行以下指令即可啟動
java -jar pubsubClient.jar

# 可指定啟動環境為測試模式
java -jar -Dspring.profiles.active=test pubsubClient.jar
```

---
# 環境變數設定
## Spring 設定
| 參數 | 預設值 | 名稱 |
|:---:|:-----:|:----:|
| SERVER_PORT | 80 | 啟動時佔用的port |
| SPRING_PROFILES_ACTIVE | dev | 啟動要用的環境變數，判斷是正式或開發或測試環境 |

## GCP 參數
| 參數 | 預設值 | 名稱 |
|:---:|:---:|:---:|
| SPRING_CLOUD_GCP_PROJECTID | nx-server-306703 | gcp project id |
| SPRING_CLOUD_GCP_CREDENTIALS_LOCATION | file:/Users/user/Downloads/google-pubsub-rhitics-production.json | token放置的位置 |
| EVOLUTIVELABS_GCP_SHIPMENT_SUBSCRIPTION | shipment.created.pipe | 訂閱者名稱 |
| EVOLUTIVELABS_GCP_SHIPMENT_PULL_SIZE | 1000 | 最大筆數 | 
| EVOLUTIVELABS_GCP_SHIPMENT_PULL_TIME | 5000 | 每一段時間抓取一次 |

## Kafka 設定
| 參數 | 預設值 | 名稱 |
|:---:|:---:|:---:|
| EVOLUTIVELABS_KAFKA_PRODUCER_TOPIC | shipment.created.pipe | topic名稱 |
| EVOLUTIVELABS_KAFKA_PRODUCER_PARTITIONS | 6 | 分區數 |
| EVOLUTIVELABS_KAFKA_PRODUCER_REPLICAS | 1 | 備份數 |
| EVOLUTIVELABS_KAFKA_BOOTSTRAP_SERVERS | 10.60.1.238:9092 | kafka位置 |

## 其餘設定
| 參數 | 預設值 | 名稱 |
|:---:|:---:|:---:|
| EVOLUTIVELABS_LOGGING_PATH | /home/user/logs/pubsubClient | log放置目錄 |