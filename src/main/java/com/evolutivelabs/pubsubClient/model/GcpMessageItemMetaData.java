package com.evolutivelabs.pubsubClient.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class GcpMessageItemMetaData implements Serializable {
    private String ccid;
    private String title;
    private String bundle_id;
    private Boolean is_bundled;
    private String package_type;
    private String hotstamp_font;
    private String hotstamp_text;
    private Boolean paste_sticker;
    private String hotstamp_color;
    private String print_image_path;
    private String preview_image_path;

    public String getCcid() {
        return ccid;
    }

    public void setCcid(String ccid) {
        this.ccid = ccid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBundle_id() {
        return bundle_id;
    }

    public void setBundle_id(String bundle_id) {
        this.bundle_id = bundle_id;
    }

    public Boolean getIs_bundled() {
        return is_bundled;
    }

    public void setIs_bundled(Boolean is_bundled) {
        this.is_bundled = is_bundled;
    }

    public String getPackage_type() {
        return package_type;
    }

    public void setPackage_type(String package_type) {
        this.package_type = package_type;
    }

    public String getHotstamp_font() {
        return hotstamp_font;
    }

    public void setHotstamp_font(String hotstamp_font) {
        this.hotstamp_font = hotstamp_font;
    }

    public String getHotstamp_text() {
        return hotstamp_text;
    }

    public void setHotstamp_text(String hotstamp_text) {
        this.hotstamp_text = hotstamp_text;
    }

    public Boolean getPaste_sticker() {
        return paste_sticker;
    }

    public void setPaste_sticker(Boolean paste_sticker) {
        this.paste_sticker = paste_sticker;
    }

    public String getHotstamp_color() {
        return hotstamp_color;
    }

    public void setHotstamp_color(String hotstamp_color) {
        this.hotstamp_color = hotstamp_color;
    }

    public String getPrint_image_path() {
        return print_image_path;
    }

    public void setPrint_image_path(String print_image_path) {
        this.print_image_path = print_image_path;
    }

    public String getPreview_image_path() {
        return preview_image_path;
    }

    public void setPreview_image_path(String preview_image_path) {
        this.preview_image_path = preview_image_path;
    }

    @Override
    public String toString() {
        return "GcpMessageItemMetaData{" +
                "ccid='" + ccid + '\'' +
                ", title='" + title + '\'' +
                ", bundle_id='" + bundle_id + '\'' +
                ", is_bundled=" + is_bundled +
                ", package_type='" + package_type + '\'' +
                ", hotstamp_font='" + hotstamp_font + '\'' +
                ", hotstamp_text='" + hotstamp_text + '\'' +
                ", paste_sticker=" + paste_sticker +
                ", hotstamp_color='" + hotstamp_color + '\'' +
                ", print_image_path='" + print_image_path + '\'' +
                ", preview_image_path='" + preview_image_path + '\'' +
                '}';
    }

    public static class GcpMessagePayLoad implements Serializable {
        private Long id;
        private Long source_id;
        private String source_type;
        private Boolean is_customized;
        private String shipping_label;
        private String tracking_number;
        private String carrier;
        private String status;
        private String invoice;
        private String notes;
        private String tags;
        private GcpMessageMetaData meta_data;

        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime estimated_delivery;

        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime shipping_label_updated_at;

        private String process_priority;
        private String ship_to_country;
        private String source_website;
        private String allocation;

        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime cancelled_at; //TODO: time

        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime paused_at;//TODO: time

        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime created_at;//TODO: time

        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime updated_at;//TODO: time

        private List<GcpMessageItem> items;
        private Boolean is_paused;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Long getSource_id() {
            return source_id;
        }

        public void setSource_id(Long source_id) {
            this.source_id = source_id;
        }

        public String getSource_type() {
            return source_type;
        }

        public void setSource_type(String source_type) {
            this.source_type = source_type;
        }

        public Boolean getIs_customized() {
            return is_customized;
        }

        public void setIs_customized(Boolean is_customized) {
            this.is_customized = is_customized;
        }

        public String getShipping_label() {
            return shipping_label;
        }

        public void setShipping_label(String shipping_label) {
            this.shipping_label = shipping_label;
        }

        public String getTracking_number() {
            return tracking_number;
        }

        public void setTracking_number(String tracking_number) {
            this.tracking_number = tracking_number;
        }

        public String getCarrier() {
            return carrier;
        }

        public void setCarrier(String carrier) {
            this.carrier = carrier;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getInvoice() {
            return invoice;
        }

        public void setInvoice(String invoice) {
            this.invoice = invoice;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public GcpMessageMetaData getMeta_data() {
            return meta_data;
        }

        public void setMeta_data(GcpMessageMetaData meta_data) {
            this.meta_data = meta_data;
        }

        public LocalDateTime getEstimated_delivery() {
            return estimated_delivery;
        }

        public void setEstimated_delivery(LocalDateTime estimated_delivery) {
            this.estimated_delivery = estimated_delivery;
        }

        public LocalDateTime getShipping_label_updated_at() {
            return shipping_label_updated_at;
        }

        public void setShipping_label_updated_at(LocalDateTime shipping_label_updated_at) {
            this.shipping_label_updated_at = shipping_label_updated_at;
        }

        public String getProcess_priority() {
            return process_priority;
        }

        public void setProcess_priority(String process_priority) {
            this.process_priority = process_priority;
        }

        public String getShip_to_country() {
            return ship_to_country;
        }

        public void setShip_to_country(String ship_to_country) {
            this.ship_to_country = ship_to_country;
        }

        public String getSource_website() {
            return source_website;
        }

        public void setSource_website(String source_website) {
            this.source_website = source_website;
        }

        public String getAllocation() {
            return allocation;
        }

        public void setAllocation(String allocation) {
            this.allocation = allocation;
        }

        public LocalDateTime getCancelled_at() {
            return cancelled_at;
        }

        public void setCancelled_at(LocalDateTime cancelled_at) {
            this.cancelled_at = cancelled_at;
        }

        public LocalDateTime getPaused_at() {
            return paused_at;
        }

        public void setPaused_at(LocalDateTime paused_at) {
            this.paused_at = paused_at;
        }

        public LocalDateTime getCreated_at() {
            return created_at;
        }

        public void setCreated_at(LocalDateTime created_at) {
            this.created_at = created_at;
        }

        public LocalDateTime getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(LocalDateTime updated_at) {
            this.updated_at = updated_at;
        }

        public List<GcpMessageItem> getItems() {
            return items;
        }

        public void setItems(List<GcpMessageItem> items) {
            this.items = items;
        }

        public Boolean getIs_paused() {
            return is_paused;
        }

        public void setIs_paused(Boolean is_paused) {
            this.is_paused = is_paused;
        }

        @Override
        public String toString() {
            return "GcpMessagePayLoad{" +
                    "id=" + id +
                    ", source_id=" + source_id +
                    ", source_type='" + source_type + '\'' +
                    ", is_customized=" + is_customized +
                    ", shipping_label='" + shipping_label + '\'' +
                    ", tracking_number='" + tracking_number + '\'' +
                    ", carrier='" + carrier + '\'' +
                    ", status='" + status + '\'' +
                    ", invoice='" + invoice + '\'' +
                    ", notes='" + notes + '\'' +
                    ", tags='" + tags + '\'' +
                    ", meta_data=" + meta_data +
                    ", estimated_delivery='" + estimated_delivery + '\'' +
                    ", shipping_label_updated_at='" + shipping_label_updated_at + '\'' +
                    ", process_priority='" + process_priority + '\'' +
                    ", ship_to_country='" + ship_to_country + '\'' +
                    ", source_website='" + source_website + '\'' +
                    ", allocation='" + allocation + '\'' +
                    ", cancelled_at='" + cancelled_at + '\'' +
                    ", paused_at='" + paused_at + '\'' +
                    ", created_at='" + created_at + '\'' +
                    ", updated_at='" + updated_at + '\'' +
                    ", items=" + items +
                    ", is_paused=" + is_paused +
                    '}';
        }
    }
}
