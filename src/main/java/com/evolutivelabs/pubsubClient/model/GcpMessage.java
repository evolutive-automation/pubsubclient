package com.evolutivelabs.pubsubClient.model;

import java.io.Serializable;

public class GcpMessage implements Serializable {
    private String name;
    private GcpMessageItemMetaData.GcpMessagePayLoad payload;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GcpMessageItemMetaData.GcpMessagePayLoad getPayload() {
        return payload;
    }

    public void setPayload(GcpMessageItemMetaData.GcpMessagePayLoad payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "GcpMessage{" +
                "name='" + name + '\'' +
                ", payload=" + payload +
                '}';
    }
}
