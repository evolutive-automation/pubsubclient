package com.evolutivelabs.pubsubClient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class PubsubClientApplication {
	public static void main(String[] args) {
		SpringApplication.run(PubsubClientApplication.class, args);
	}

}
