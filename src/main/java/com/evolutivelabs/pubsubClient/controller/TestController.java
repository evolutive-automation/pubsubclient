package com.evolutivelabs.pubsubClient.controller;

import com.evolutivelabs.pubsubClient.config.EvolutivelabsProperties;
import com.evolutivelabs.pubsubClient.model.GcpMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.spring.pubsub.core.PubSubTemplate;
import com.google.cloud.spring.pubsub.support.AcknowledgeablePubsubMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestController {
    private static final Logger logger = LoggerFactory.getLogger(TestController.class);
    @Autowired
    private PubSubTemplate pubSubTemplate;
    @Autowired
    private EvolutivelabsProperties evolutivelabsProperties;
    @Autowired
    private KafkaTemplate kafkaTemplate;
    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping("/push")
    public String push(@RequestParam String value) {
        try {
            kafkaTemplate.send(evolutivelabsProperties.getProducer().getTopic(), "0", value);
        } catch (Throwable t) {
            logger.error(t.getMessage(), t);
        }
        return "success";
    }

    @GetMapping("/pull")
    public String pull() {
        List<AcknowledgeablePubsubMessage> list = pubSubTemplate.pull(evolutivelabsProperties.getSubscription(), 1000, true);
        logger.info("size: {}", list.size());
        list.forEach(message -> {
            String data = message.getPubsubMessage().getData().toStringUtf8();
            try {
                GcpMessage gcpMessage = objectMapper.readValue(data, GcpMessage.class);
                kafkaTemplate.send(evolutivelabsProperties.getProducer().getTopic(), String.valueOf(gcpMessage.getPayload().getId()), data);
            } catch (JsonProcessingException e) {
                kafkaTemplate.send(evolutivelabsProperties.getProducer().getTopic(), "0", data);
            }
            message.ack();
        });
        return "success";
    }

    @GetMapping("/context")
    public String context() {
        for (String name : applicationContext.getBeanDefinitionNames()) {
            logger.info("bean Name: {}, class: {}", name, applicationContext.getBean(name).getClass());
        }
        return "success";
    }
}