package com.evolutivelabs.pubsubClient.controller;

import com.evolutivelabs.pubsubClient.config.EvolutivelabsProperties;
import com.evolutivelabs.pubsubClient.model.GcpMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.spring.pubsub.core.PubSubTemplate;
import com.google.cloud.spring.pubsub.support.AcknowledgeablePubsubMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/api/admin")
@RestController
public class AdminController {
    private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
    private final ApplicationContext ctx;

    public AdminController(ApplicationContext ctx) {
        this.ctx = ctx;
    }

    @PostMapping("/shutdown")
    public void closeApplication(HttpServletRequest request) {
        logger.info("shutdown with user addr : {}", request.getRemoteAddr());
        ((ConfigurableApplicationContext) ctx).close();
    }


}
