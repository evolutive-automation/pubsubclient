package com.evolutivelabs.pubsubClient.schedule;

import com.evolutivelabs.pubsubClient.config.EvolutivelabsProperties;
import com.evolutivelabs.pubsubClient.model.GcpMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.spring.pubsub.core.PubSubTemplate;
import com.google.cloud.spring.pubsub.support.AcknowledgeablePubsubMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GcpTopicListener {
    private static final Logger logger = LoggerFactory.getLogger(GcpTopicListener.class);
    private final EvolutivelabsProperties evolutivelabsProperties;
    private final PubSubTemplate pubSubTemplate;
    private final KafkaTemplate kafkaTemplate;
    private final ObjectMapper objectMapper;

    public GcpTopicListener(PubSubTemplate pubSubTemplate,
                            EvolutivelabsProperties evolutivelabsProperties,
                            KafkaTemplate kafkaTemplate,
                            ObjectMapper objectMapper) {
        this.pubSubTemplate = pubSubTemplate;
        this.evolutivelabsProperties = evolutivelabsProperties;
        this.kafkaTemplate = kafkaTemplate;
        this.objectMapper = objectMapper;
    }


    /**
     * 從gcp抓取message，並丟入kafka
     * 定時
     */
    @Scheduled(fixedDelayString = "${evolutivelabs.gcp.shipment.pull.time}")
    public void pullFromGCPAndSendToKafka() {
        List<AcknowledgeablePubsubMessage> list = pubSubTemplate.pull(evolutivelabsProperties.getSubscription(),
                evolutivelabsProperties.getSize(),
                true);
        logger.info("gcp pub/sub pull message size: {}", list.size());
        list.forEach(message -> {
            String data = message.getPubsubMessage().getData().toStringUtf8();
            try {
                GcpMessage gcpMessage = objectMapper.readValue(data, GcpMessage.class);
                kafkaTemplate.send(evolutivelabsProperties.getProducer().getTopic(),
                        String.valueOf(gcpMessage.getPayload().getId()), data);
            } catch (JsonProcessingException e) {
                kafkaTemplate.send(evolutivelabsProperties.getProducer().getTopic(), "0", data);
            }

            message.ack(); //ack之後sub不會再重新取得
        });
    }
}
