package com.evolutivelabs.pubsubClient.config;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class EvoKafkaConfig {
    private final EvolutivelabsProperties evolutivelabsProperties;
    public EvoKafkaConfig(EvolutivelabsProperties evolutivelabsProperties) {
        this.evolutivelabsProperties = evolutivelabsProperties;
    }

    @Bean
    public NewTopic topic() {
        return TopicBuilder.name(evolutivelabsProperties.getProducer().getTopic())
                .partitions(evolutivelabsProperties.getProducer().getPartitions())
                .replicas(evolutivelabsProperties.getProducer().getReplicas())
                .build();
    }

    @Bean
    public ProducerFactory producerFactory() {
        Map<String, Object> producerConfig = new HashMap<>();
        producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, evolutivelabsProperties.getBootstrapServers());
        producerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        producerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        producerConfig.put(ProducerConfig.RECONNECT_BACKOFF_MS_CONFIG, 1000);
        producerConfig.put(ProducerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG, 10000);
        producerConfig.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, 10000);
        return new DefaultKafkaProducerFactory(producerConfig);
    }


    @Bean
    public KafkaAdmin admin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, evolutivelabsProperties.getBootstrapServers());
        return new KafkaAdmin(configs);
    }
}
