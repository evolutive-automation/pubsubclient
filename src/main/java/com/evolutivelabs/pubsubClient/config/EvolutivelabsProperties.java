package com.evolutivelabs.pubsubClient.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Component
@ConfigurationProperties("evolutivelabs.kafka")
public class EvolutivelabsProperties {
    @Value("${evolutivelabs.gcp.shipment.subscription}")
    private String subscription;
    @Value("${evolutivelabs.gcp.shipment.pull.size}")
    private Integer size;

    @Value("${evolutivelabs.kafka.bootstrap.servers}")
    private String bootstrapServers;

    private EvolutivelabsProperties.Producer producer;

    public String getSubscription() {
        return subscription;
    }

    public Integer getSize() {
        return size;
    }

    public String getBootstrapServers() {
        return bootstrapServers;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public static class Producer {
        private String topic;
        private Integer partitions;
        private Integer replicas;

        public void setTopic(String topic) {
            this.topic = topic;
        }

        public void setPartitions(Integer partitions) {
            this.partitions = partitions;
        }

        public void setReplicas(Integer replicas) {
            this.replicas = replicas;
        }

        public String getTopic() {
            return topic;
        }

        public Integer getPartitions() {
            return partitions;
        }

        public Integer getReplicas() {
            return replicas;
        }

    }
}
